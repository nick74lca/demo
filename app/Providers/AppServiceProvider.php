<?php

namespace App\Providers;

use App\Services\SocialFacebookAccountService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind( 'App\Services\SocialFacebookAccountService',
        function( $app, array $parameters)
        {
            //call the constructor passing the first element of $parameters
            return new SocialFacebookAccountService($parameters[0]);
        } );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

<?php


namespace App\Services;


use App\Base\ThirdPartyLoginBase;
use App\Models\SocialFacebookAccount;
use App\Models\User;
use Laravel\Socialite\Contracts\User as ProviderUser;
use Laravel\Socialite\Facades\Socialite;

class SocialFacebookAccountService extends ThirdPartyLoginBase
{
    public function __construct($type)
    {
        $this->type = $type;
    }

    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialFacebookAccount::whereProvider($this->type)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        $user = null;
        if ($account) {
            $user = $account->user;
        } else {
            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $this->type
            ]);
            $user = User::whereEmail($providerUser->getEmail())->first();
            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'password' => md5(rand(1, 10000)),
                ]);
            }
            $account->user()->associate($user);
            $account->save();
            $user = $user;
        }
        $this->user = $user;
    }

    function callback()
    {
        $this->createOrGetUser(Socialite::driver($this->type)->user());
    }
}
